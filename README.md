# Smart Home System using Node.js

This is smart home system using Node.js, Express, Mongoose, Handlebars and some other packages.

### Version: 1.0.0

### Usage

```sh
$ npm install
```

```sh
$ npm start
# And run css compile
$ npm run watch

# Visit http://localhost:3000
```

## License

Copyright © 2020 **Van Dam** - [nvan23](https://gitlab.com/nvan23)