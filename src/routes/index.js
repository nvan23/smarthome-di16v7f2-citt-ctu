const siteRouter = require('./site.route');

function route(app) {
  // Add other routes

  app.use('/', siteRouter);
}

module.exports = route;
